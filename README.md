<h1 align= "center"> Code Challenge | Calculadora de Tinta </h1>

# Descrição
<p> Esse projeto vai te auxiliar a calcular de maneira mais rápida e prática a quantidade de tinta necessária para pintar o seu ambiente. </p>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/195478976@N05/52034100879/in/dateposted-public/" title="_C__Users_Computador_Documents_code-challenge_index.html (2)"><img src="https://live.staticflickr.com/65535/52034100879_3e8ec6ce7f.jpg" width="500" height="187" alt="_C__Users_Computador_Documents_code-challenge_index.html (2)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

--------------------------------------------

# Como executar?
* Baixe o projeto em seu equipamento
* Abra o mesmo através de um IDE ([VS Code](https://code.visualstudio.com/))
* Em seguida execute o arquivo HTML em seu navegador. 

<a data-flickr-embed="true" href="https://www.flickr.com/photos/195478976@N05/52034369215/in/dateposted-public/" title="_C__Users_Computador_Documents_code-challenge_index.html (1)"><img src="https://live.staticflickr.com/65535/52034369215_c217e3cec0.jpg" width="500" height="339" alt="_C__Users_Computador_Documents_code-challenge_index.html (1)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
